import { useState } from 'react'
import './App.css'
import ClassComp from './components/ClassComp'
import FunctionComp from './components/FunctionComp'

function App() {
	const [tab, setTab] = useState('classComp')
	function changeView() {
		if (tab === 'classComp') {
			return <ClassComp />
		} else {
			return <FunctionComp />
		}
	}
	return (
		<div className='App'>
			<div>
				<button
					onClick={() => setTab('classComp')}
					type='button'
					className={`btn m-3 '  ${
						tab === 'classComp' ? 'btn-primary' : 'btn-secondary'
					}`}
				>
					classComponent
				</button>
				<button
					onClick={() => setTab('funcComp')}
					type='button'
					className={`btn m-3 '  ${
						tab === 'funcComp' ? 'btn-primary' : 'btn-secondary'
					}`}
				>
					functionalComponent
				</button>
			</div>
			{changeView()}
		</div>
	)
}

export default App
