import React, { useState, useEffect } from 'react'

export default function FunctionComp() {
	const [post, setPost] = useState('')
	useEffect(() => {
		console.log('Welcome to Functional component')
		fetchPost()
		return () => {
			console.log('ByeBye Functional component')
			setPost('')
		}
	}, [])

	async function fetchPost() {
		try {
			const data = await fetch('https://randomuser.me/api')
			const postData = await data.json()
			setPost(postData.results[0])
		} catch (error) {
			console.log(error)
		}
	}
	return (
		<div className='hello'>
			<div className='card text-white bg-dark m-3' style={{ width: '18rem' }}>
				<img
					src={post && post.picture.large}
					className='card-img-top'
					alt={post && post.name.first}
				/>
				<div className='card-body'>
					<h5 className='card-title'>
						{post && `${post.name.title}  ${post.name.last} ${post.name.first}`}
					</h5>
					<p className='card-text'>{post && `${post.email}`}</p>
				</div>
			</div>
			<button className='btn btn-success m-3' onClick={fetchPost}>
				new user
			</button>
		</div>
	)
}
