import { Component } from 'react'

class ClassComp extends Component {
	state = {
		count: 0,
	}

	componentWillUnmount() {
		console.log('ByeBye Class component')
	}
	componentDidMount() {
		console.log('Welcome to Class component')
	}
	increment = () => {
		this.setState({
			count: this.state.count < 10 ? this.state.count + 1 : this.state.count,
		})
	}
	decrement = () => {
		this.setState({
			count: this.state.count > 0 ? this.state.count - 1 : this.state.count,
		})
	}
	render() {
		return (
			<div>
				<button className='btn btn-warning m-3' onClick={this.increment}>
					increment
				</button>
				<span className='badge bg-secondary lh-lg m-3'>{this.state.count}</span>
				<button className='btn btn-danger m-3' onClick={this.decrement}>
					decrement
				</button>
			</div>
		)
	}
}
export default ClassComp
